package com.huixian.apollo.logging;

import com.ctrip.framework.apollo.Config;
import com.ctrip.framework.apollo.model.ConfigChange;
import com.ctrip.framework.apollo.spring.annotation.ApolloConfig;
import com.huixian.apollo.ApolloPropertiesChangeEvent;
import java.util.Set;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.logging.LogLevel;
import org.springframework.boot.logging.LoggingSystem;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * Description : 支持logging.level日志打印级别的动态修改  .
 *
 *
 * 注解 @Order(Integer.MIN_VALUE) 是用于提前（spring context初始化时）logging level级别的设置。不加也可以。
 * </br>
 * {@link LoggingApolloPropertiesChangeEvent#initLoggingLevel}，主要调用的是此方法进行初始化，然而这样并不完美：spring context容器启动到此方法执行之间的log日志的打印级别还是不受控制。
 *
 * @author : zhangyouwen
 * @date : Created in 2019/7/11 10:04
 */
@Slf4j
@Component
@Order(Integer.MIN_VALUE)
public class LoggingApolloPropertiesChangeEvent implements ApolloPropertiesChangeEvent {

    private static final String LOGGER_TAG = "logging.level.";

    @Autowired
    private LoggingSystem loggingSystem;

    @ApolloConfig
    private Config config;

    /**
     * 初始化log level
     */
    @PostConstruct
    private void initLoggingLevel() {
        Set<String> keys = config.getPropertyNames();
        for (String key : keys) {
            if (containsIgnoreCase(key, LOGGER_TAG)) {
                String loggerName = key.replace(LOGGER_TAG, "");
                String logLevel = config.getProperty(key, LogLevel.OFF.name());
                loggingSystem.setLogLevel(loggerName, LogLevel.valueOf(logLevel.toUpperCase()));
                log.info("Initialize log level, key: [{}], level: [{}]", key, logLevel);
            }
        }
    }

    @Override
    public void add(ConfigChange change) {
        String key = change.getPropertyName();
        if (containsIgnoreCase(key, LOGGER_TAG)) {
            String loggerName = key.replace(LOGGER_TAG, "");
            String newValue = change.getNewValue();
            loggingSystem.setLogLevel(loggerName, LogLevel.valueOf(newValue.toUpperCase()));
            log.info("Change log level, add key: [{}], new value: [{}]", key, newValue);
        }
    }

    @Override
    public void delete(ConfigChange change) {
        String key = change.getPropertyName();
        if (containsIgnoreCase(key, LOGGER_TAG)) {
            loggingSystem.setLogLevel(key.replace(LOGGER_TAG, ""), LogLevel.OFF);
            log.info("Change log level off, key: [{}], old value: [{}]", key, change.getOldValue());
        }
    }

    @Override
    public void modified(ConfigChange change) {
        String key = change.getPropertyName();
        if (containsIgnoreCase(key, LOGGER_TAG)) {
            String loggerName = key.replace(LOGGER_TAG, "");
            String newValue = change.getNewValue();
            loggingSystem.setLogLevel(loggerName, LogLevel.valueOf(newValue.toUpperCase()));
            log.info("Change log level, key: [{}], new value: [{}], old value: [{}]", key, newValue, change.getOldValue());
        }
    }

    private static boolean containsIgnoreCase(String str, String searchStr) {
        if (str == null || searchStr == null) {
            return false;
        }
        int len = searchStr.length();
        int max = str.length() - len;
        for (int i = 0; i <= max; i++) {
            if (str.regionMatches(true, i, searchStr, 0, len)) {
                return true;
            }
        }
        return false;
    }

}
