package com.huixian.apollo;

import com.ctrip.framework.apollo.model.ConfigChange;
import com.ctrip.framework.apollo.model.ConfigChangeEvent;

/**
 * Description :   .
 *
 * @author : zhangyouwen
 * @date : Created in 2019/4/19 17:23
 */
public interface ApolloPropertiesChangeEvent {

    /**
     * apollo 配置变化，调用入口
     *
     * @param changeEvent changeEvent
     */
    default void invoke(ConfigChangeEvent changeEvent) {
        for (String key : changeEvent.changedKeys()) {
            ConfigChange change = changeEvent.getChange(key);
            switch (change.getChangeType()) {
                case ADDED:
                    add(change);
                    break;
                case MODIFIED:
                    modified(change);
                    break;
                case DELETED:
                    delete(change);
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * apollo 配置变化：新增配置
     *
     * @param change ConfigChange
     */
    default void add(ConfigChange change) {
    }

    /**
     * apollo 配置变化：删除配置
     *
     * @param change ConfigChange
     */
    default void delete(ConfigChange change) {
    }

    /**
     * apollo 配置变化：修改配置
     *
     * @param change ConfigChange
     */
    default void modified(ConfigChange change) {
    }

}
