package com.huixian.apollo;

import com.ctrip.framework.apollo.ConfigService;
import com.ctrip.framework.apollo.model.ConfigChange;
import com.ctrip.framework.apollo.model.ConfigChangeEvent;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.environment.EnvironmentChangeEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;

/**
 * Description :   .
 *
 * @author : zhangyouwen
 * @date : Created in 2019/4/10 18:52
 */
@Slf4j
@Configuration
public class ApolloPropertiesRefresher implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Value("${apollo.bootstrap.namespaces}")
    private String[] namespaces;

    /**
     * 为所有的namespace配置变化添加监听（服务启动执行一次） 注意如果apollo.bootstrap.namespaces这个配置有变化，相应的namespace监听不会有变化
     */
    @PostConstruct
    private void addAllNamespaceChangeListener() {
        //监听所有的namespace
        for (String namespace : namespaces) {
            addChangeListener(namespace);
        }
    }

    private void addChangeListener(String namespace) {
        log.info("添加apollo namespace: [{}]的监听", namespace);
        //config instance is singleton for each namespace and is never null
        ConfigService.getConfig(namespace).addChangeListener(changeEvent -> {
            printChangeLog(changeEvent);
            refreshApolloProperties(changeEvent);
        });
    }

    private void printChangeLog(ConfigChangeEvent changeEvent){
        for (String key : changeEvent.changedKeys()) {
            ConfigChange change = changeEvent.getChange(key);
            log.info("Found properties change - namespace: {}, key: {}, oldValue: {}, newValue: {}, changeType: {}, changeEvent: {}",
                    changeEvent.getNamespace(),
                    change.getPropertyName(),
                    change.getOldValue(),
                    change.getNewValue(),
                    change.getChangeType(),
                    changeEvent.getChange(key).toString());
        }
    }

    private void refreshApolloProperties(ConfigChangeEvent changeEvent) {
        log.info("Refreshing apollo properties!");

        /**
         * 更新相应@ConfigurationProperties注解的bean的属性值
         *
         * rebind configuration beans, e.g. com.huixian.alpharisk.config.properties.Xinyan
         *
         * The application will listen for an EnvironmentChangeEvent and react to the change in a couple of standard ways (additional ApplicationListeners can be added as @Beans by the user in the normal way). When an EnvironmentChangeEvent is observed it will have a list of key values that have changed, and the application will use those to:
         *
         * 1.Re-bind any @ConfigurationProperties beans in the context
         *
         * 2.Set the logger levels for any properties in logging.level.*, this worked for Spring Cloud Environment
         * @see org.springframework.cloud.context.properties.ConfigurationPropertiesRebinder#onApplicationEvent
         */
        applicationContext.publishEvent(new EnvironmentChangeEvent(changeEvent.changedKeys()));

        /**
         * apollo配置修改，会执行ApolloPropertiesChangeEvent接口的所有实现类的invoke方法
         * 如果你想在apollo配置修改时做一些具体业务，只需实现ApolloPropertiesChangeEvent接口，而不必关注调用链
         */
        applicationContext.getBeansOfType(ApolloPropertiesChangeEvent.class).values().stream().forEach(change -> change.invoke(changeEvent));

        log.info("Apollo properties refreshed!");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

}