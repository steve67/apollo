package com.huixian.apollo.datasource;

import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.metrics.micrometer.MicrometerMetricsTrackerFactory;
import io.micrometer.core.instrument.MeterRegistry;
import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * @author demo
 * @see <a href="https://github.com/ctripcorp/apollo-use-cases/tree/master/dynamic-datasource"></a>
 */
@Slf4j
@Component
public class DataSourceManager {

    @Autowired
    private DataSourceProperties dataSourceProperties;

    @Autowired
    private ApplicationContext applicationContext;

    private volatile MeterRegistry meterRegistry;

    /**
     * create a hikari data source
     *
     * @see org.springframework.boot.autoconfigure.jdbc.DataSourceConfiguration.Hikari#dataSource
     */
    public HikariDataSource createDataSource() {
        HikariDataSource dataSource = dataSourceProperties.initializeDataSourceBuilder().type(HikariDataSource.class).build();
        if (StringUtils.hasText(dataSourceProperties.getName())) {
            dataSource.setPoolName(dataSourceProperties.getName());
        }
        //bind metrics
        //因为RefreshableDataSource extends HikariDataSource，spring容器启动时，通过代理拿到的datasource进行prometheus metrics绑定，并不是真正的datasource。
        //所以这里提前进行绑定，这样prometheus才能监控到数据源
        this.bindMetricsRegistryToHikariDataSource(this.getMeterRegistry(), dataSource);
        return dataSource;
    }

    /**
     * 获取spring容器中的Prometheus的meter registry，如果有的话。
     * 只有注册了MeterRegistry，prometheus才能做监控
     *
     * @return MeterRegistry
     */
    private MeterRegistry getMeterRegistry() {
        if (this.meterRegistry == null) {
            try {
                this.meterRegistry = this.applicationContext.getBean(MeterRegistry.class);
            } catch (Exception e) {
                // 未接入prometheus
            }
        }
        return this.meterRegistry;
    }

    private void bindMetricsRegistryToHikariDataSource(MeterRegistry registry, HikariDataSource dataSource) {
        if (registry == null) {
            // 未接入prometheus
            return;
        }
        if (dataSource.getMetricRegistry() == null && dataSource.getMetricsTrackerFactory() == null) {
            try {
                dataSource.setMetricsTrackerFactory(new MicrometerMetricsTrackerFactory(registry));
            } catch (Exception ex) {
                log.warn("Failed to bind Hikari metrics: " + ex.getMessage());
            }
        }
    }

    public HikariDataSource createAndTestDataSource() {
        // dataSource此时为空
        HikariDataSource newDataSource = createDataSource();
        try {
            // 真正的初始化
            testConnection(newDataSource);
        } catch (SQLException ex) {
            log.error("Testing connection for data source failed: {}", newDataSource.getJdbcUrl(), ex);
        }
        return newDataSource;
    }

    private void testConnection(DataSource dataSource) throws SQLException {
        //borrow a connection
        Connection connection = dataSource.getConnection();
        //return the connection
        connection.close();
    }

}
