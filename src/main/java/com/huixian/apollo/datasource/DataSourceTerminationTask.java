package com.huixian.apollo.datasource;

import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.HikariPoolMXBean;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.sql.DataSource;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author demo
 * @see <a href="https://github.com/ctripcorp/apollo-use-cases/tree/master/dynamic-datasource"></a>
 */
@Slf4j
public class DataSourceTerminationTask implements Runnable {

    /**
     * 最大重试次数
     */
    private static final int MAX_RETRY_TIMES = 10;

    /**
     * 重试的时间间隔，单位毫秒
     */
    private static final int RETRY_DELAY_IN_MILLISECONDS = 5000;

    /**
     * 已经重新的次数
     */
    private volatile int retryTimes;

    /**
     * 需要关闭的数据源
     */
    private final DataSource dataSourceToTerminate;

    /**
     * 定时调度线程池
     */
    private final ScheduledExecutorService scheduledExecutorService;

    public DataSourceTerminationTask(DataSource dataSourceToTerminate,
            ScheduledExecutorService scheduledExecutorService) {
        this.dataSourceToTerminate = dataSourceToTerminate;
        this.scheduledExecutorService = scheduledExecutorService;
        this.retryTimes = 0;
    }

    @Override
    public void run() {
        // 尝试关闭数据源
        if (terminate(dataSourceToTerminate)) {
            log.info("Data source {} terminated successfully!", dataSourceToTerminate);
        } else {
            // 数据源没有成功关闭，则通过定时调度线程池重新调度
            scheduledExecutorService.schedule(this, RETRY_DELAY_IN_MILLISECONDS, TimeUnit.MILLISECONDS);
        }
    }

    /**
     * 尝试关闭数据源
     *
     * @param dataSource 要关闭的数据源
     * @return boolean
     */
    private boolean terminate(DataSource dataSource) {
        log.info("Trying to terminate data source: {}", dataSource);
        try {
            if (dataSource instanceof HikariDataSource) {
                return terminateHikariDataSource((HikariDataSource) dataSource);
            }

            log.info("Not supported data source: {}", dataSource);
            log.error("Not supported data source: {}", dataSource);

            return true;
        } catch (Throwable ex) {
            log.warn("Terminating data source {} failed, will retry in {} ms, error message: {}, e:",
                    dataSource, RETRY_DELAY_IN_MILLISECONDS, ex.getMessage(), ex);
            return false;
        } finally {
            retryTimes++;
        }
    }

    /**
     * 关闭Hikari数据源
     *
     * @param dataSource HikariDataSource
     * @return boolean
     * @see <a href="https://github.com/brettwooldridge/HikariCP/issues/742">Support graceful shutdown of connection pool</a>
     */
    private boolean terminateHikariDataSource(HikariDataSource dataSource) {

        HikariPoolMXBean poolMXBean = dataSource.getHikariPoolMXBean();

        if (poolMXBean == null) {
            dataSource.close();
            return true;
        }

        //evict idle connections
        poolMXBean.softEvictConnections();

        if (poolMXBean.getActiveConnections() > 0 && retryTimes < MAX_RETRY_TIMES) {
            log.warn("Data source {} still has {} active connections, will retry in {} ms.",
                    dataSource, poolMXBean.getActiveConnections(), RETRY_DELAY_IN_MILLISECONDS);
            return false;
        }

        if (poolMXBean.getActiveConnections() > 0) {
            log.warn("Retry times({}) >= {}, force closing data source {}, with {} active connections!",
                    retryTimes, MAX_RETRY_TIMES, dataSource, poolMXBean.getActiveConnections());
        }

        dataSource.close();

        return true;
    }

}
