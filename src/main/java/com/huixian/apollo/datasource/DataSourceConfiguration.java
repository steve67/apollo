package com.huixian.apollo.datasource;

import javax.sql.DataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author demo
 * @see <a href="https://github.com/ctripcorp/apollo-use-cases/tree/master/dynamic-datasource"></a>
 */
@Slf4j
@Configuration
public class DataSourceConfiguration {

    @Bean
    public RefreshableDataSource dataSource(DataSourceManager dataSourceManager) {
        log.info("Create refreshable data source ... ");
        DataSource actualDataSource = dataSourceManager.createDataSource();
        return new RefreshableDataSource(actualDataSource);
    }

}
