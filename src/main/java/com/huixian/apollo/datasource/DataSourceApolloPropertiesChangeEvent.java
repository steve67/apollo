package com.huixian.apollo.datasource;

import com.ctrip.framework.apollo.model.ConfigChange;
import com.ctrip.framework.apollo.model.ConfigChangeEvent;
import com.google.common.collect.Sets;
import com.huixian.apollo.ApolloPropertiesChangeEvent;
import com.huixian.common2.util.EnvironmentUtil;
import java.util.Set;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.sql.DataSource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

/**
 * Description : 支持数据源配置（url, username, password）的动态修改 .
 *
 * @author : zhangyouwen
 * @date : Created in 2019/7/8 15:59
 */
@Slf4j
@Component
public class DataSourceApolloPropertiesChangeEvent implements ApolloPropertiesChangeEvent {

    private static final String DATASOURCE_URL = "spring.datasource.url";
    private static final String DATASOURCE_USERNAME = "spring.datasource.username";
    private static final String DATASOURCE_PASSWORD = "spring.datasource.password";
    private static final Set<String> DATASOURCE_CONFIG = Sets.newHashSet(DATASOURCE_URL, DATASOURCE_USERNAME, DATASOURCE_PASSWORD);

    /**
     * ScheduledThreadPoolExecutor 定时任务线程池
     */
    private static ScheduledExecutorService scheduledExecutorService = new ScheduledThreadPoolExecutor(1,
            new BasicThreadFactory.Builder().namingPattern("datasource-terminate-schedule-pool").daemon(true).build());

    @Autowired
    private DataSourceManager dataSourceManager;

    @Autowired
    private RefreshableDataSource refreshableDataSource;

    @Override
    public void invoke(ConfigChangeEvent changeEvent) {
        Set<String> changedKeys = changeEvent.changedKeys();
        // 如果有数据库url、username、password的配置变化，则重新创建新的数据源
        if (CollectionUtils.containsAny(changedKeys, DATASOURCE_CONFIG)) {
            // 线上环境，不允许修改
            if (EnvironmentUtil.isPrd()) {
                log.info("Refreshing data source: FORBIDDEN! ");
                return;
            }
            log.info("Refreshing data source");
            // 打印log
            printLog(changeEvent);
            // 根据最新的数据库配置，创建新数据源
            DataSource newDataSource = dataSourceManager.createAndTestDataSource();
            // 替换spring context中的数据源为新数据源，并获取旧数据源
            DataSource oldDataSource = refreshableDataSource.setAndGetDataSource(newDataSource);
            // 通过异步线程池，关闭旧数据源
            asyncTerminateOldDataSource(oldDataSource);
            log.info("Finished refreshing data source");
        }
    }

    /**
     * 打印日志
     *
     * @param changeEvent ConfigChangeEvent
     */
    private void printLog(ConfigChangeEvent changeEvent) {
        Set<String> changedKeys = changeEvent.changedKeys();
        ConfigChange configChange;
        if (changedKeys.contains(DATASOURCE_URL)) {
            configChange = changeEvent.getChange(DATASOURCE_URL);
            log.info("Change hikari-datasource config: [{}], new value: [{}], old value: [{}]", DATASOURCE_URL, configChange.getNewValue(), configChange.getOldValue());
        }
        if (changedKeys.contains(DATASOURCE_USERNAME)) {
            configChange = changeEvent.getChange(DATASOURCE_USERNAME);
            log.info("Change hikari-datasource config: [{}], new value: [{}], old value: [{}]", DATASOURCE_USERNAME, configChange.getNewValue(), configChange.getOldValue());
        }
        if (changedKeys.contains(DATASOURCE_PASSWORD)) {
            configChange = changeEvent.getChange(DATASOURCE_PASSWORD);
            log.info("Change hikari-datasource config: [{}], new value: [{}], old value: [{}]", DATASOURCE_PASSWORD, configChange.getNewValue(), configChange.getOldValue());
        }
    }

    /**
     * 通过异步线程池，关闭旧数据源
     *
     * @param oldDataSource 要关闭的旧数据源
     */
    private void asyncTerminateOldDataSource(DataSource oldDataSource) {
        DataSourceTerminationTask task = new DataSourceTerminationTask(oldDataSource, scheduledExecutorService);
        //start now
        scheduledExecutorService.schedule(task, 0, TimeUnit.MILLISECONDS);
    }

}
