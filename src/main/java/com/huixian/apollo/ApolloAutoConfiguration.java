package com.huixian.apollo;

import com.huixian.apollo.datasource.DataSourceApolloPropertiesChangeEvent;
import com.huixian.apollo.datasource.DataSourceConfiguration;
import com.huixian.apollo.datasource.DataSourceManager;
import com.huixian.apollo.gateway.DynamicRoutingApolloPropertiesChangeEvent;
import com.huixian.apollo.logging.LoggingApolloPropertiesChangeEvent;
import com.huixian.common2.util.PropertiesUtil;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.gateway.config.GatewayProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;

/**
 * create at  2018/08/22 10:53
 *
 * @author yanggang
 */
@Configuration
@ConditionalOnProperty(name = "apollo.bootstrap.enabled", havingValue = "true")
@Import(value = {
        ApolloPropertiesRefresher.class,
        PropertiesUtil.class
})
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class ApolloAutoConfiguration {

    @Configuration
    @ConditionalOnClass(HikariDataSource.class)
    @ConditionalOnProperty(name = "spring.datasource.type", havingValue = "com.zaxxer.hikari.HikariDataSource")
    @Import({
            DataSourceApolloPropertiesChangeEvent.class,
            DataSourceManager.class,
            DataSourceConfiguration.class,
    })
    static class DataSourceAutoConfiguration {

    }


    @Configuration
    @Import({
            LoggingApolloPropertiesChangeEvent.class,
    })
    static class LoggingAutoConfiguration {

    }


    @Configuration
    static class GatewayAutoConfiguration {

        @Configuration
        @ConditionalOnClass(GatewayProperties.class)
        @Import({
                DynamicRoutingApolloPropertiesChangeEvent.class,
        })
        protected static class GatewayDynamicRoutingAutoConfiguration {

        }

    }

}
